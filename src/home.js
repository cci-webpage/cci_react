import React, { Component } from 'react';
import { Card, Row, Col, Container, Image, Carousel } from 'react-bootstrap';
import Header from './header';
import { FaUserCheck, FaAndroid } from "react-icons/fa";
import { MdWeb } from "react-icons/md";
import head from './images/shape-1.png'
import kerala from './images/kerala.png'
import fisat from './images/fisat.png'
import creative from './images/creative.png'
import gxtron from './images/gxtron.webp'
import fisat1 from './images/fisatian.png'
import petsday from './images/petsday.png'
import canteen from './images/canteen.png'
import mokkanoor from './images/mokkanoor.png'
class Main extends Component {
  render() {
    return (
      <div>
        <Row style={{ backgroundColor: '#f7f7f7', width: 'auto', height: 'auto' }}>

          <Col className="d-none d-lg-block" xs="12" sm="6" style={{ paddingTop: 170 }}>

            <h1 style={{ fontSize: '5rem', fontWeight: 'bold', paddingLeft: 70, paddingTop: 70 }}>

              We Make
                </h1>
            <h1 style={{ fontSize: '5rem', fontWeight: 'bold', color: '#0f64bd', paddingTop: 20, paddingLeft: 70 }}>

              Things Possible
                </h1>

          </Col>
          <Col className="d-lg-none" xs="12" sm="6" style={{ paddingTop: 170 }}>

            <h1 style={{ fontSize: '5vh', fontWeight: 'bold', paddingLeft: '3rem' }}>

              We Make
                </h1>
            <h1 style={{ fontSize: '5vh', fontWeight: 'bold', color: '#0f64bd', paddingTop: 10, paddingLeft: 40 }}>

              Things Possible
                </h1>

          </Col>
          <Col className="d-none d-lg-block">
            <Image src={head} style={{ paddingTop: 120, width: '75%' }} rounded />
          </Col>
          <Col className="d-block d-sm-none" xs={1}>
          </Col>
          <Col className="d-lg-none">
            <Image src={head} style={{ paddingTop: 120, width:'20rem'}} rounded />
          </Col>
        </Row>

        <Row className="justify-content-md-center" style={{ paddingTop: 140 }}>
          <Col className="d-none d-lg-block" sm={2} lg={2}>
            <h2 style={{ fontWeight: 'bold' }}> Our Best Services</h2>
          </Col>
        </Row>
        
        <Row className="justify-content-md-center">

          <Col className="d-lg-none" xs={12} md={6} style={{paddingLeft:'4rem'}}>
            <h2 style={{ fontWeight: 'bold' }}> Our Best Services</h2>
          </Col>
        </Row>

        {/* <Row className="justify-content-md-center" style={{ paddingTop: 20 }}>
          <Col className="d-none d-lg-block" xs="16" >
            <h5 style={{ color: '#aba9a4' }}>Subtech's full suite APIs enable teams to develop unique search.</h5>
          </Col>
        </Row>
        <Row className="justify-content-md-center" style={{ paddingTop: 20 }}>
          <Col className="d-lg-none" xs="12" sm="4" md={9} style={{ paddingLeft: 25 }}>
            <h5 style={{ color: '#aba9a4' }}>Subtech's full suite APIs enable teams to develop unique search.</h5>
          </Col>
        </Row> */}

        <Container style={{ paddingBottom: 100,paddingLeft:30}}>
          <Row className="justify-content-md-center" style={{ paddingTop: 60 }}>
            <Col>
              <Card style={{ width: '18rem',marginTop:'1rem',minHeight:'15rem' }}>
                <Col style={{ margin: 'auto', paddingTop: 8 }} xs={3} md={4}>
                  <h1>
                    <FaUserCheck style={{ color: 'blue' }} />
                  </h1>
                </Col>
                <Card.Body>
                  <Card.Title style={{ fontWeight: 'bold', paddingLeft: 'auto' }}>UI / UX</Card.Title>
                  <Card.Text style={{ color: '#6e6c69' }}>
                  User experience design (UXD, UED, or XD) is the process of manipulating user behavior through usability, usefulness, and desirability provided in the interaction with a product.
    </Card.Text>

                </Card.Body>
              </Card>
            </Col>
            <Col>
              <Card style={{ width: '18rem',marginTop:'1rem' }}>
                <Col style={{ margin: 'auto', paddingTop: 8 }} xs={4} md={4}>
                  <h1>
                    <FaAndroid style={{ color: 'blue' }} />
                  </h1>
                </Col>
                <Card.Body>
                  <Card.Title style={{ fontWeight: 'bold' }}>Android Development</Card.Title>
                  <Card.Text style={{ color: '#6e6c69' }}>
                  Android software development is the process by which new applications are created for devices running the Android operating system. Android is first in the market
    </Card.Text>
                </Card.Body>

              </Card>
            </Col>
            <br/>

            <br/>
            <Col >
              <Card style={{ width: '18rem',marginTop:'1rem',minHeight:'18rem'}}>
                <Col style={{ margin: 'auto', paddingTop: 8 }} xs={4} md={4}>
                  <h1>
                    <MdWeb style={{ color: 'blue' }} />
                  </h1>
                </Col>
   
                <Card.Body>
                  <Card.Title style={{ fontWeight: 'bold' }}>Web Development</Card.Title>
                  <Card.Text style={{ color: '#6e6c69' }}>
                  Web development is the work involved in developing a website for the Internet or an intranet. 
    </Card.Text>
                </Card.Body>

              </Card>
            </Col>
          </Row>
        </Container>



        {/*<-----------------------for the desktop view----------------------------------------->*/}
        <div className="d-none d-lg-block" style={{ backgroundColor: '#f0f2f0', height: '60rem',width:'auto' }}>
          <Row className="justify-content-md-center" style={{ paddingTop: 70 }}>
            <Col className="d-none d-sm-block" xs="16">
              <h2 style={{ fontWeight: 'bold' }}> Our Sucess Stories</h2>
            </Col>
          </Row>
         
          <Container style={{ paddingTop: 40, paddingBottom: 40 }}>
            <Carousel style={{ color: 'black' }}>
              <Carousel.Item >
                <Card style={{ width: '50rem', margin: 'auto' }}>
                  <Card.Img variant="top" src={fisat1} />
                  <Card.Body style={{ height: '14rem' }}>
                    <Card.Title style={{ paddingTop: 20, fontWeight: 'bold' }}>Fisatian(Phase 1)</Card.Title>
                    <Card.Text style={{ paddingTop: 20 }}>
                      It is a college management system for FISAT
                        college.This application uses Django as backend and Progressive
                             Web App(PWA) as frontend created using HTML5+CSS3+JS.
    </Card.Text>

                  </Card.Body>
                </Card>
              </Carousel.Item>
              <Carousel.Item>
                <Card style={{ width: '50rem', margin: 'auto' }}>
                  <Card.Img variant="top" src={petsday} />
                  <Card.Body style={{ height: '14rem' }}>
                    <Card.Title style={{ paddingTop: 20, fontWeight: 'bold' }}>PetsDay Website Redesign:</Card.Title>
                    <Card.Text style={{ paddingTop: 20 }}>
                      Petsday is an online platform to buy and sell pets. We provided a
  new design for their online site. The redesign was done using figma.
    </Card.Text>
                  </Card.Body>
                </Card>
              </Carousel.Item>
              <Carousel.Item >
                <Card style={{ width: '50rem', margin: 'auto' }}>
                  <Card.Img variant="top" style={{ maxHeight: '50rem' }} src={canteen} />
                  <Card.Body style={{ height: '14rem' }}>
                    <Card.Title style={{ paddingTop: 20, fontWeight: 'bold' }}>FISAT Canteen management system:</Card.Title>
                    <Card.Text style={{ paddingTop: 20 }}>
                      This system is used to digitize the college canteen.
                      <br />
                      <p>It can be used to
    • Preordering of food
    • Tabs on user payments
    • Auto Payment for the staff
    </p>
                    </Card.Text>

                  </Card.Body>
                </Card>
              </Carousel.Item>
              <Carousel.Item >
                <Card style={{ width: '50rem', margin: 'auto' }}>
                  <Card.Img variant="top" style={{ maxHeight: '50rem' }} src={mokkanoor} />
                  <Card.Body style={{ height: '14rem' }}>
                    <Card.Title style={{ paddingTop: 20, fontWeight: 'bold' }}>Mookkannoor Gramapanchayath app:</Card.Title>
                    <Card.Text style={{ paddingTop: 20 }}>
                      An information app for the Mookkannoor Gramapanchayath. It also
                         has an option to submit feedback and report about street lights
                             which are not working.
    </Card.Text>

                  </Card.Body>
                </Card>
              </Carousel.Item>
            </Carousel>
          </Container>
        </div>
        {/*<-----------------------for the mobile view----------------------------------------->*/}
        <div className="d-block d-sm-block d-md-none" style={{ backgroundColor: '#f0f2f0', height: '40rem' }}>
          <Row className="justify-content-md-center" style={{ paddingTop: 70 }}>
            
            <Col className="d-lg-none" sm={8}>
              <h2 style={{ fontWeight: 'bold',paddingLeft:40 }}> Our Sucess Stories</h2>
            </Col>
          </Row>

          <Container xs={12} style={{ paddingTop: 40, paddingBottom: 40 }}>
            <Carousel style={{ color: 'black' }}>
              <Carousel.Item >
                <Card style={{ width: 'auto', margin: 'auto' }}>
                  <Card.Img variant="top" src={fisat1} />
                  <Card.Body style={{ height: '14rem' }}>
                    <Card.Title style={{ paddingTop: 20, fontWeight: 'bold' }}>Fisatian(Phase 1)</Card.Title>
                    <Card.Text style={{ paddingTop: 20 }}>
                      It is a college management system for FISAT
                        college.This application uses Django as backend and Progressive
                             Web App(PWA) as frontend created using HTML5+CSS3+JS.
    </Card.Text>

                  </Card.Body>
                </Card>
              </Carousel.Item>
              <Carousel.Item>
              <Card style={{ width: 'auto', margin: 'auto' }}>
                  <Card.Img variant="top" src={petsday} />
                  <Card.Body style={{ height: '14rem' }}>
                    <Card.Title style={{ paddingTop: 20, fontWeight: 'bold' }}>PetsDay Website Redesign:</Card.Title>
                    <Card.Text style={{ paddingTop: 20 }}>
                      Petsday is an online platform to buy and sell pets. We provided a
  new design for their online site. The redesign was done using figma.
    </Card.Text>
                  </Card.Body>
                </Card>
              </Carousel.Item>
              <Carousel.Item >
              <Card style={{ width: 'auto', margin: 'auto' }}>
                  <Card.Img variant="top" style={{ maxHeight: '50rem' }} src={canteen} />
                  <Card.Body style={{ height: '14rem' }}>
                    <Card.Title style={{ paddingTop: 20, fontWeight: 'bold' }}>Canteen management system:</Card.Title>
                    <Card.Text style={{ paddingTop: 20 }}>
                      This system is used to digitize the college canteen.
                      <br />
                      <p>It can be used to
    • Preordering of food
    • Tabs on user payments
    • Auto Payment for the staff
    </p>
                    </Card.Text>

                  </Card.Body>
                </Card>
              </Carousel.Item>
              <Carousel.Item >
              <Card style={{ width: 'auto', margin: 'auto' }}>
                  <Card.Img variant="top" style={{ maxHeight: '50rem' }} src={mokkanoor} />
                  <Card.Body style={{ height: '14rem' }}>
                    <Card.Title style={{ paddingTop: 20, fontWeight: 'bold' }}>Mookkannoor Gramapanchayath app:</Card.Title>
                    <Card.Text style={{ paddingTop: 20 }}>
                      An information app for the Mookkannoor Gramapanchayath. It also
                         has an option to submit feedback and report about street lights
                             which are not working.
    </Card.Text>

                  </Card.Body>
                </Card>
              </Carousel.Item>
            </Carousel>
          </Container>
        </div>
        {/*<----------------------MD---------------------------------------------------->*/}
        <div className="d-none d-md-block d-lg-none" style={{ backgroundColor: '#f0f2f0', height: '50rem' }}>
          <Row className="justify-content-md-center" style={{ paddingTop: 70 }}>
            <Col xs={1} >
            </Col>
            <Col className="d-lg-none" xs="16">
              <h2 style={{ fontWeight: 'bold' }}> Our Sucess Stories</h2>
            </Col>
          </Row>

          <Container xs={12} style={{ paddingTop: 40, paddingBottom: 40 }}>
            <Carousel style={{ color: 'black' }}>
              <Carousel.Item >
                <Card style={{ width: 'auto', margin: 'auto' }}>
                  <Card.Img variant="top" src={fisat1} />
                  <Card.Body style={{ height: '14rem' }}>
                    <Card.Title style={{ paddingTop: 20, fontWeight: 'bold' }}>Fisatian(Phase 1):</Card.Title>
                    <Card.Text style={{ paddingTop: 20 }}>
                      It is a college management system for FISAT
                        college.This application uses Django as backend and Progressive
                             Web App(PWA) as frontend created using HTML5+CSS3+JS.
    </Card.Text>

                  </Card.Body>
                </Card>
              </Carousel.Item>
              <Carousel.Item>
              <Card style={{ width: 'auto', margin: 'auto' }}>
                  <Card.Img variant="top" src={petsday} />
                  <Card.Body style={{ height: '10rem' }}>
                    <Card.Title style={{ paddingTop: 20, fontWeight: 'bold' }}>PetsDay Website Redesign:</Card.Title>
                    <Card.Text style={{ paddingTop: 20 }}>
                      Petsday is an online platform to buy and sell pets. We provided a
  new design for their online site. The redesign was done using figma.
    </Card.Text>
                  </Card.Body>
                </Card>
              </Carousel.Item>
              <Carousel.Item >
              <Card style={{ width: 'auto', margin: 'auto' }}>
                  <Card.Img variant="top" style={{ maxHeight: '50rem' }} src={canteen} />
                  <Card.Body style={{ height: '14rem' }}>
                    <Card.Title style={{ paddingTop: 20, fontWeight: 'bold' }}>FISAT Canteen management system:</Card.Title>
                    <Card.Text style={{ paddingTop: 20 }}>
                      This system is used to digitize the college canteen.
                      <br />
                      <p>It can be used to
    • Preordering of food
    • Tabs on user payments
    • Auto Payment for the staff
    </p>
                    </Card.Text>

                  </Card.Body>
                </Card>
              </Carousel.Item>
              <Carousel.Item >
              <Card style={{ width: 'auto', margin: 'auto' }}>
                  <Card.Img variant="top" style={{ maxHeight: '50rem' }} src={mokkanoor} />
                  <Card.Body style={{ height: '14rem' }}>
                    <Card.Title style={{ paddingTop: 20, fontWeight: 'bold' }}>Mookkannoor Gramapanchayath app:</Card.Title>
                    <Card.Text style={{ paddingTop: 20 }}>
                      An information app for the Mookkannoor Gramapanchayath. It also
                         has an option to submit feedback and report about street lights
                             which are not working.
    </Card.Text>

                  </Card.Body>
                </Card>
              </Carousel.Item>
            </Carousel>
          </Container>
        </div>




        <Row className="justify-content-md-center" style={{ paddingTop: 70 }}>
          <Col className="d-none d-lg-block" xs="16">
            <h2 style={{ fontWeight: 'bold' }}> Our Clients</h2>
          </Col>
        </Row>
        <Row className="justify-content-md-center" style={{ paddingTop: 20 }}>
          <Col className="d-block d-sm-none" xs={3}>
          </Col>
         
        </Row>
        <Container className="d-none d-lg-block" style={{ paddingBottom: 50, paddingTop: 60 }}>
          <Row>
            <Col xs={5} md={3}>
              <Image src={kerala} rounded />
            </Col>
            <Col xs={5} md={3}>
              <Image src={fisat} rounded />
            </Col>
            <Col xs={5} md={4}>
              <Image src={creative} rounded />
            </Col>
            <Col xs={2} md={2}>
              <Image src={gxtron} rounded />
            </Col>
          </Row>
        </Container>
        {/*<-------------------for the mobile view----------------->*/}
        <Container className="d-lg-none" style={{ paddingBottom: 50, paddingTop: 20 }}>
          <Row>
            <Col style={{paddingLeft:110}} xs={12} sm={12} md={6}>
              <Image src={kerala} rounded />
            </Col>
            <br/>
            <Col style={{paddingLeft:70,paddingTop:20}} xs={12} sm={12} md={6}>
              <Image src={fisat} rounded />
            </Col>
            <br/>
            <Col style={{paddingLeft:60,paddingTop:20}} xs={12} sm={12} md={6}>
              <Image src={creative} rounded />
            </Col>
            <br/>
            <Col style={{paddingLeft:90,paddingTop:20}} xs={12} sm={12} md={6}>
              <Image style={{margin:"auto"}} src={gxtron} rounded />
            </Col>

          </Row>
        </Container>

      </div>


    );
  }
}

export default Main;