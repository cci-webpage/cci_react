import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Row, Col, Image,Form, Button } from 'react-bootstrap';
import {
  NavItem,
  NavLink
} from 'reactstrap';
import contact from './contact';
import Header from './nav';
import Login from './login';
import Team from './team';
import  About from './about';
import Hire from './hire';
import  Home from './home';
import './App.css';
import  Week_code from './week_code';
import logo from './images/logo.png';
import { MdEmail, MdLocalPhone, MdMap, MdCode,MdSubscriptions } from "react-icons/md";
import { FaFacebookSquare,FaInstagram,FaTwitter,FaSlackHash,FaLinkedin} from "react-icons/fa";

class App extends Component {
  render() {
    return (
     
      <Router>

        <div className="App">
          <Header />
          <Route exact path="/about" component={About} />
          <Route exact path="/contact" component={contact} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/team" component={Team} />
          <Route exact path="/week_code" component={Week_code} />
          <Route exact path="/" component={Home} />
          <Route exact path="/hire" component={Hire} />
        </div>
        <footer className="footer" style={{padding:'auto'}}>

          <Row>
            {/*--------------mobile----------------*/}
            <Col className="d-block d-sm-block d-md-none" xs={12} md={4}>
              <Image src={logo} style={{ paddingTop:'2rem',paddingBottom:15 }} rounded />

              <h5 style={{paddingTop:10}}>
              <FaLinkedin/> <FaTwitter/> <FaSlackHash/>

              </h5>
            </Col>
            {/*----------Desktop---------*/}
            <Col className="d-none d-lg-block" xs={12} md={4}>
              <Image src={logo} style={{ paddingTop:'4rem ',paddingBottom:15 }} rounded />

              <h5 style={{paddingTop:10}}>
                 <FaLinkedin/> <FaTwitter/> <FaSlackHash/>

              </h5>
            </Col>
            {/*-----------MD------------*/}
            <Col className="d-none d-md-block d-lg-none" xs={12} md={4}>
              <Image src={logo} style={{ paddingTop:'5rem',paddingBottom:15 }} rounded />

              <h5 style={{paddingTop:10}}>
                 <FaLinkedin/> <FaTwitter/> <FaSlackHash/>

              </h5>
            </Col>
            <Col className="d-none d-sm-block" xs={6} md={3} style={{ paddingTop: '4rem' }}>
              <h5 style={{ fontWeight: 'bold' }}>
                Contact us  <MdCode />
              </h5>
              <Row>
                <Col style={{ paddingTop: 20 }} md={12} sm={3} lg={14} xs={14}>
                  <h6>
                    <MdEmail /> ccifisat2018@gmail.com
                </h6>
                  <h6 style={{ paddingTop: 20 }}>
                    <MdLocalPhone /> 9847772490
                </h6>
                  <h6 style={{ paddingTop: 20, textAlign: 'center' }}>
                    <MdMap /> Hormis Nagar, Mookkannoor,Kerala
                </h6>
                </Col>
              </Row>
            </Col>
            <Col className="d-none d-sm-block" xs={6} md={2} style={{ paddingTop: '4rem' }}>
              <h5 style={{ fontWeight: 'bold' }}>
                Links <MdCode />
              </h5>
              <NavLink style={{color:'white'}} href="/">Home</NavLink>
              <NavLink style={{color:'white'}} href="/team">Team</NavLink>
              <NavLink style={{color:'white'}} href="/contact">Contact Us</NavLink>

            </Col>
            <Col className="d-none d-sm-block" xs={12} md={2} style={{ paddingTop: '4rem',paddingBottom:10 }}>
              <h5 style={{ fontWeight: 'bold' }}>
                Sign up for updates <MdSubscriptions />
              </h5>
              <Form style={{paddingTop:20}}>
                <Form.Group controlId="formBasicEmail">
                  <Form.Control type="email" placeholder="Enter email" />
                </Form.Group>
              </Form>
              <Button style={{fontWeight:'bold'}} variant="secondary">Subscribe</Button>{' '}
            </Col>
          </Row>
        </footer>
          <footer className="foot">
            <Row>
              <Col xs={12} md={12}>
                <h10 style={{ color: '#6e6e78' }}>
                  Copyright © 2020
          </h10>
              </Col>
            </Row>

          </footer>

      </Router>

        )
      }
    }
    
    export default App;
const icon_1={
  paddingLeft:"20"
}
