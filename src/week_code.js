import React, { Component } from 'react';
import { Card, Row, Col, Container, Image, Alert, Button, ListGroup } from 'react-bootstrap';
import Landing from "./images/code.png";
import './week.css';
import html from './images//html.png';
import css from './images/css.png';
import js from './images/js.png';
import week from './images/week.png';
class App extends Component {
    render() {
        return (
            <>
                <div className="test">

                    <Container>
                        <div className="d-none d-lg-block">
                            <Col style={{ paddingTop: 140, paddingBottom: 50 }}>

                                <div class="landing-page-full row pt-5 mx-0">
                                    <div className="col text-center">
                                        <img src={Landing} style={{ paddingRight: 50 }} className="img-fluid mx-auto" ></img>
                                    </div>
                                    <div className="col align-self-center">
                                        <p>
                                            <h1 style={{ letterSpacing: "1rem", color: 'white', fontSize: '5rem', paddingTop: '4rem', fontWeight: 'bold' }}>
                                                WEEK OF CODING
                    </h1>
                                        </p>
                                    </div>
                                </div>
                            </Col>
                        </div>

                        <div className="d-block d-sm-block d-md-none">
                            <Col style={{ paddingTop: 100, paddingBottom: 50 }}>

                                <div class="landing-page-full row pt-5 mx-0">

                                    <div className="col align-self-center">
                                        <p>
                                            <h1 style={{ letterSpacing: "1rem", color: 'white', fontSize: '3rem', paddingTop: '4rem', fontWeight: 'bold' }}>
                                                WEEK OF CODING
                    </h1>
                                        </p>
                                    </div>
                                    <div className="col text-center">
                                        <img src={Landing} style={{ paddingRight: 50, paddingTop: 30 }} className="img-fluid mx-auto" ></img>
                                    </div>
                                </div>
                            </Col>

                            <div className="d-none .d-xl-block">
                                <Col style={{ paddingTop: 100, paddingBottom: 50 }}>

                                    <div class="landing-page-full row pt-5 mx-0">

                                        <div className="col align-self-center">
                                            <p>
                                                <h1 style={{ letterSpacing: "1rem", color: 'white', fontSize: '3rem', paddingTop: '4rem', fontWeight: 'bold' }}>
                                                    WEEK OF CODING
                    </h1>
                                            </p>
                                        </div>
                                        <div className="col text-center">
                                            <img src={Landing} style={{ paddingRight: 50, paddingTop: 30 }} className="img-fluid mx-auto" ></img>
                                        </div>
                                    </div>
                                </Col>
                            </div>

                        </div>
                    </Container>

                    <Row className="justify-content-center" style={{ paddingBottom: '5rem', paddingTop: '2rem' }}>
                        
                            <Button variant="success" style={{ fontSize: '2rem' }}><b>Closed</b></Button>
                        
                    </Row>
            
                </div>

                <Container style={{ paddingTop: '7rem' }}>
                    <h1 style={{ textAlign: 'center' }}><b>| About The Event </b></h1>
                    <Row className="justify-content-center">
                        <Alert variant="light">

                            <Alert.Heading style={{ color: 'black', fontSize: '2rem', paddingTop: 30 }}>Let us spend the time off from college in a creative and fruitful manner.</Alert.Heading>
                            <h4 style={{ paddingTop: 20 }}>
                                "The Week of Code, organized by Centre for Cyber Innovations in Association with FISAT, is a free
                                 to attend online workshop for design of dynamic and interactive web pages."

                        </h4>
                            <hr />
                        </Alert>
                    </Row>

                    <Row className="justify-content-center" style={{ paddingTop: '3rem' }}>
                        <h1><b>Domains</b></h1>
                    </Row>
                    <Row className="justify-content-md-center" style={{ paddingTop: 60 }}>
                        <Col>
                            <Card style={{ width: '18rem', marginTop: '1rem', minHeight: '15rem' }}>
                                <Col style={{ margin: 'auto', paddingTop: 8 }} xs={4} md={5}>
                                    <h1>
                                        <Image src={html} rounded />
                                    </h1>
                                </Col>
                                <Card.Body>
                                    <Card.Title style={{ fontWeight: 'bold', paddingLeft: 'auto' }}>HTML</Card.Title>
                                    <Card.Text style={{ color: '#6e6c69' }}>
                                        Hypertext Markup Language is the standard markup language for documents designed to be displayed in a web browser.
    </Card.Text>

                                </Card.Body>
                            </Card>
                        </Col>
                        <Col>
                            <Card style={{ width: '18rem', marginTop: '1rem' }}>
                                <Col style={{ margin: 'auto', paddingTop: 8 }} xs={4} md={4}>
                                    <h1>
                                        <Image src={css} rounded />
                                    </h1>
                                </Col>
                                <Card.Body>
                                    <Card.Title style={{ fontWeight: 'bold' }}>CSS</Card.Title>
                                    <Card.Text style={{ color: '#6e6c69' }}>
                                        Cascading Style Sheets is a style sheet language used for describing the presentation of a document written in a markup language like HTML.
    </Card.Text>
                                </Card.Body>

                            </Card>
                        </Col>
                        <br />

                        <br />
                        <Col >
                            <Card style={{ width: '18rem', marginTop: '1rem', minHeight: '18rem' }}>
                                <Col style={{ margin: 'auto', paddingTop: 8 }} xs={4} md={4}>
                                    <h1>
                                        <Image src={js} rounded />
                                    </h1>
                                </Col>

                                <Card.Body>
                                    <Card.Title style={{ fontWeight: 'bold' }}>Java Script</Card.Title>
                                    <Card.Text style={{ color: '#6e6c69' }}>
                                        JavaScript is a programming language commonly used in web development. It was originally developed to add dynamic and interactive elements to websites.
    </Card.Text>
                                </Card.Body>

                            </Card>
                        </Col>
                    </Row>

                    <div className="d-none d-lg-block">
                        <Row className="justify-content-center" style={{ paddingTop: '3rem' }}>
                            <h1><b>What to Expect</b></h1>
                        </Row>
                        <Row className="justify-content-center" style={{ paddingTop: '3rem', paddingBottom: 40 }}>
                            <Image style={{ width: '45%' }} src={week} rounded />
                        </Row>

                    </div>

                    <Row className="justify-content-center" style={{ paddingTop: '3rem' }}>
                        <h1 style={{ textAlign: 'center' }}><b>Access the Project</b></h1>
                    </Row>

                    <div className="d-none d-lg-block" >
                        <Row className="justify-content-md-center" style={{ paddingTop: '4rem' }}>
                            <Col xs lg="1">
                            </Col>

                            <Col xs lg="4">
                                <Alert variant="success">
                                    <Alert.Heading>GitHub</Alert.Heading>
                                    <p>
                                        We are using GitHub for providing the resources, examples and Assignment. We will be
                                        provide contents for each day via GitHub.
  </p>
                                    <hr />
                                    <p className="mb-0">
                                        <a href="https://github.com/CCIFISAT/week_of_coding">

                                            <button type="button" class="btn btn-outline-success and-all-other-classes">
                                                Click
                                    </button>
                                        </a>
                                    </p>
                                </Alert>
                            </Col>
                            <Col xs lg="4">
                                <Alert variant="success">
                                    <Alert.Heading>Git Cheat Sheet</Alert.Heading>
                                    <p>
                                        Git cheat sheet that serves as a quick reference for basic Git commands to help
                                        you learn Git. Git branches, remote repositories, undoing changes, and more.

  </p>
                                    <hr />
                                    <p className="mb-0">
                                        <a href="https://education.github.com/git-cheat-sheet-education.pdf">

                                            <button type="button" class="btn btn-outline-success and-all-other-classes">
                                                Click
                                    </button>
                                        </a>
                                    </p>
                                </Alert>
                            </Col>

                        </Row>
                    </div>
                    <div className="d-block d-sm-block d-md-none" style={{ paddingTop: '3rem' }}>
                        <Col>
                            <Alert variant="success">
                                <Alert.Heading>GitHub</Alert.Heading>
                                <p>
                                    We are using GitHub for providing the resources, examples and Assignment. We will be
                                    provide contents for each day via GitHub.
  </p>
                                <hr />
                                <p className="mb-0">
                                    <a href="https://github.com/CCIFISAT/week_of_coding">

                                        <button type="button" class="btn btn-outline-success and-all-other-classes">
                                            Click
                                    </button>
                                    </a>
                                </p>
                            </Alert>
                        </Col>
                        <Col>
                            <Alert variant="success">
                                <Alert.Heading>Git Cheat Sheet</Alert.Heading>
                                <p>
                                    Git cheat sheet that serves as a quick reference for basic Git commands to help
                                    you learn Git. Git branches, remote repositories, undoing changes, and more.

  </p>
                                <hr />
                                <p className="mb-0">
                                    <a href="https://education.github.com/git-cheat-sheet-education.pdf">

                                        <button type="button" class="btn btn-outline-success and-all-other-classes">
                                            Click
                                    </button>
                                    </a>
                                </p>
                            </Alert>
                        </Col>
                    </div>

                    <Row className="justify-content-center" style={{ paddingTop: '8rem' }}>
                        <h1 style={{ textAlign: 'center' }}><b>Things to Notice</b></h1>
                    </Row>

                    <Row className="justify-content-center" style={{ paddingTop: '3rem' }}>
                        <Card style={{ width: '28rem' }}>
                            <ListGroup variant="flush">
                                <ListGroup.Item><b>1. Contents and Assignment will be released each day.</b></ListGroup.Item>
                                <ListGroup.Item><b>2. Clone every day project before you start learning. </b></ListGroup.Item>
                                <ListGroup.Item><b>3. You can run the examples on any editor eg (visual Studio, text editor etc) on your PC</b></ListGroup.Item>
                                <ListGroup.Item><b>4. Go through every examples and contents that make it easier to submit daily assignment</b></ListGroup.Item>
                                <ListGroup.Item><b>5. A detailed video is provided about how to use Git</b></ListGroup.Item>
                                <ListGroup.Item><b>6. While submitting assignment create your own branch in CCI repo,  and use your name as branch name.</b></ListGroup.Item>
                                <ListGroup.Item><b>7. Submit your assignment before evening. Successful submissions will be merged.</b> </ListGroup.Item>
                                <ListGroup.Item><b>8. Don't worry if you are not able to submit the assignment. Solution for each day Assignment will be available in Github by evening.</b></ListGroup.Item>
                            </ListGroup>
                        </Card>
                    </Row>

                    <Row className="justify-content-center" style={{ paddingTop: '8rem' }}>
                        <h1 style={{ textAlign: 'center' }}><b>Getting Started With Git</b></h1>
                    </Row>
                    <Row className="justify-content-center" style={{ paddingTop: '3rem', paddingBottom: '3rem', paddingLeft: 10 }}>
                        <iframe width="580" height="315" src="https://www.youtube.com/embed/zMgrv4PFt3M" frameborder="0"
                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </Row>
                        
                        <Row className="justify-content-center" style={{ paddingTop: '2rem', paddingBottom: '3rem', paddingLeft: 10 }}>
                       <iframe width="580" height="315" src="https://www.youtube.com/embed/jQiSOmQrq-A" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </Row>

                    <div className="d-none d-lg-block" style={{ paddingBottom: '4rem' }}>
                        <Container>
                            <Row className="justify-content-center" style={{ paddingTop: '3rem', paddingBottom: '3rem' }}>
                                <h1><b>| Contact Us</b></h1>
                            </Row>
                            <Row className="justify-content-center">
                                <Col style={{ color: '#001987' }} xs lg="4">
                                    <h1>__________</h1>
                                </Col>
                                <Col style={{ color: '#001987' }} xs lg="4">
                                    <h1>__________</h1>
                                </Col>
                                <Col style={{ color: '#001987' }}>
                                    <h1>__________</h1>
                                </Col>
                            </Row>
                            <Row className="justify-content-center">


                                <Col xs lg="4">
                                    <h3><b>Paul Elias Sojan</b></h3>
                                    <h5 style={{ paddingTop: 10 }}>8281895254</h5>
                                    <h6>paul.eliassojan@gmail.com</h6>
                                </Col>
                                <Col xs lg="4">
                                    <h3><b>Mohit Rajan E</b></h3>
                                    <h5 style={{ paddingTop: 10 }}>9037969499</h5>
                                    <h6>mhit98@gmail.com</h6>
                                </Col>
                                <Col>
                                    <h3><b>Devu R Kurup</b></h3>
                                    <h6 style={{ paddingTop: 10 }}>9526701774</h6>
                                    <h6>devukurup199918@gmail.com</h6>
                                </Col>

                            </Row>
                        </Container>
                    </div>

                    <div className="d-block d-sm-block d-md-none">
                        <Row className="justify-content-center" style={{ paddingTop: '3rem', paddingBottom: '3rem' }}>
                            <h1><b>| Contact Us</b></h1>
                        </Row>
                        <Row className="justify-content-center">

                            <Col style={{ textAlign: 'center' }}>
                                <h1>______________</h1>
                                <h3><b>Paul Elias Sojan</b></h3>
                                <h6 style={{ paddingTop: 10 }}>8281895254</h6>
                                <h6>paul.eliassojan@gmail.com</h6>
                            </Col>
                            <Col style={{ textAlign: 'center' }}>
                                <h1>______________</h1>
                                <h3><b>Mohit Rajan E</b></h3>
                                <h6 style={{ paddingTop: 10 }}>9037969499</h6>
                                <h6>mhit98@gmail.com</h6>
                            </Col>
                            <Col style={{ textAlign: 'center' }}>
                                <h1>______________</h1>
                                <h3><b>Devu R Kurup</b></h3>
                                <h6 style={{ paddingTop: 10 }}>9526701774</h6>
                                <h6>devukurup199918@gmail.com</h6>
                                <h1>______________</h1>
                            </Col>
                        </Row>
                    </div>

                </Container>


            </>
        );
    };
}
export default App;
