import React from 'react';
import { Card, Image, Col, Row, Container } from 'react-bootstrap';
import { FaLinkedin, FaGithub, FaTwitter } from "react-icons/fa";
import 'bootstrap/dist/css/bootstrap.min.css';
import asset from './images/our_team.png';
import leo from './images/leo.png';
import mohit from './images/mohit.jpg';
import paul from './images/paul.jpg';
import alen from './images/alen.jpg';
import naveen from './images/naveen.jpeg';
import devu from './images/devu.jpg';
import gokul from './images/gokul.jpeg';
import deepak from './images/deepak.jpg';
import Gallery from './gallery.png';

const Example = (props) => {
  return (
    <div style={teamStyle}>
      <Row>
        <Col sm={4} lg={1} md={8} style={{ paddingLeft: '3rem' }}>
          <Image style={{ paddingTop: 20 }} src={asset} rounded />
        </Col>
      </Row>
      <Container className="d-none d-lg-block" style={{ paddingLeft: 40, paddingBottom: 50 }}>{/* <-------- For the desktop view-------->*/}
        <Row style={{ paddingTop: 80 }}>
          <Col xs={4}>
            <Card style={{ width: '18rem' }}>
              <Col style={{ paddingLeft: 50, paddingTop: 13 }} xs={6} md={4}>
                <Image src={leo} roundedCircle />
              </Col>
              <Card.Body>
                <Card.Title style={{ color: '#080924', fontWeight: 'bold', paddingLeft: 50 }}>Leo Varghese</Card.Title>
                <Container style={{ paddingTop: 20 }}>
                  <Row>
                    <Col>
                      <a
                        href="https://www.linkedin.com/in/leo-mv-578b13147/"
                      >
                        <h4 style={{ color: 'black' }}>
                          <FaLinkedin />
                        </h4>
                      </a>

                    </Col>
                    <Col>
                      <a
                        href="https://gitlab.com/leo_george"
                      >
                        <h4 style={{ color: 'black' }}>
                          <FaGithub />
                        </h4>
                      </a>
                    </Col>
                    <Col>
                      <a
                        href="https://twitter.com/Leomv55"
                      >
                        <h4 style={{ color: 'black' }}>
                          <FaTwitter />
                        </h4>
                      </a>
                    </Col>
                  </Row>
                </Container>


              </Card.Body>

            </Card>
          </Col>
          <Col sx={5}>
            <Card style={{ width: '18rem' }}>
              <Col style={{ paddingLeft: 50, paddingTop: 13 }} xs={6} md={4}>
                <Image src={mohit} roundedCircle />
              </Col>
              <Card.Body>
                <Card.Title style={{ color: '#45ad97', fontWeight: 'bold', paddingLeft: 50 }}>Mohit Rajan E</Card.Title>
                <Container style={{ paddingTop: 20 }}>
                  <Row>
                    <Col>
                      <a
                        href="https://www.youtube.com/c/jamesqquick"
                      >
                        <h4 style={{ color: 'black' }}>
                          <FaLinkedin />
                        </h4>
                      </a>

                    </Col>
                    <Col>
                      <a
                        href="https://www.youtube.com/c/jamesqquick"
                      >
                        <h4 style={{ color: 'black' }}>
                          <FaGithub />
                        </h4>
                      </a>
                    </Col>
                    <Col>
                      <a
                        href="https://www.youtube.com/c/jamesqquick"
                      >
                        <h4 style={{ color: 'black' }}>
                          <FaTwitter />
                        </h4>
                      </a>
                    </Col>
                  </Row>
                </Container>
              </Card.Body>

            </Card>
          </Col>
          <Col sx={5}>
            <Card style={{ width: '18rem' }}>
              <Col style={{ paddingLeft: 50, paddingTop: 13 }} xs={6} md={4}>
                <Image src={alen} roundedCircle />
              </Col>
              <Card.Body>
                <Card.Title style={{ color: '#45ad97', fontWeight: 'bold', paddingLeft: 50 }}>Alan Loovees</Card.Title>
                <Container style={{ paddingTop: 20 }}>
                  <Row>
                    <Col>
                      <a
                        href="https://www.linkedin.com/in/alan-loovees-472916169 "
                      >
                        <h4 style={{ color: 'black' }}>
                          <FaLinkedin />
                        </h4>
                      </a>

                    </Col>
                    <Col>
                      <a
                        href="https://gitlab.com/AlanLoovees"
                      >
                        <h4 style={{ color: 'black' }}>
                          <FaGithub />
                        </h4>
                      </a>
                    </Col>
                    <Col>
                      <a
                        href="https://twitter.com/alanloovees"
                      >
                        <h4 style={{ color: 'black' }}>
                          <FaTwitter />
                        </h4>
                      </a>
                    </Col>
                  </Row>
                </Container>

              </Card.Body>

            </Card>
          </Col>
        </Row>
        <Row style={{ paddingTop: 50 }}>
          <Col xs={4}>
            <Card style={{ width: '18rem' }}>
              <Col style={{ paddingLeft: 50, paddingTop: 13 }} xs={6} md={4}>
                <Image src={deepak} roundedCircle />
              </Col>
              <Card.Body>
                <Card.Title style={{ color: '#45ad97', fontWeight: 'bold', paddingLeft: 25 }}>Deepak Rao Fletcher</Card.Title>
                <Container style={{ paddingTop: 20 }}>
                  <Row>
                    <Col>
                      <a
                        href="https://www.youtube.com/c/jamesqquick"
                      >
                        <h4 style={{ color: 'black' }}>
                          <FaLinkedin />
                        </h4>
                      </a>

                    </Col>
                    <Col>
                      <a
                        href="https://www.youtube.com/c/jamesqquick"
                      >
                        <h4 style={{ color: 'black' }}>
                          <FaGithub />
                        </h4>
                      </a>
                    </Col>
                    <Col>
                      <a
                        href="https://www.youtube.com/c/jamesqquick"
                      >
                        <h4 style={{ color: 'black' }}>
                          <FaTwitter />
                        </h4>
                      </a>
                    </Col>
                  </Row>
                </Container>
              </Card.Body>

            </Card>
          </Col>
          <Col>
            <Card style={{ width: '18rem' }}>
              <Col style={{ paddingLeft: 50, paddingTop: 13 }} xs={6} md={4}>
                <Image src={devu} roundedCircle />
              </Col>
              <Card.Body>
                <Card.Title style={{ color: '#45ad97', fontWeight: 'bold', paddingLeft: 50 }}>Devu R Kurup</Card.Title>
                <Container style={{ paddingTop: 20 }}>
                  <Row>
                    <Col>
                      <a
                        href="https://www.youtube.com/c/jamesqquick"
                      >
                        <h4 style={{ color: 'black' }}>
                          <FaLinkedin />
                        </h4>
                      </a>

                    </Col>
                    <Col>
                      <a
                        href="https://www.youtube.com/c/jamesqquick"
                      >
                        <h4 style={{ color: 'black' }}>
                          <FaGithub />
                        </h4>
                      </a>
                    </Col>
                    <Col>
                      <a
                        href="https://www.youtube.com/c/jamesqquick"
                      >
                        <h4 style={{ color: 'black' }}>
                          <FaTwitter />
                        </h4>
                      </a>
                    </Col>
                  </Row>
                </Container>
              </Card.Body>

            </Card>
          </Col>
          <Col>
            <Card style={{ width: '18rem' }}>
              <Col style={{ paddingLeft: 50, paddingTop: 13 }} xs={6} md={4}>
                <Image src={paul} roundedCircle />
              </Col>
              <Card.Body>
                <Card.Title style={{ color: '#45ad97', fontWeight: 'bold', paddingLeft: 50 }}>Paul Elias Sojan</Card.Title>
                <Container style={{ paddingTop: 20 }}>
                  <Row>
                    <Col>
                      <a
                        href="https://www.linkedin.com/in/paul-elias-sojan-8b9b80177/"
                      >
                        <h4 style={{ color: 'black' }}>
                          <FaLinkedin />
                        </h4>
                      </a>

                    </Col>
                    <Col>
                      <a
                        href="https://gitlab.com/paul.eliassojan"
                      >
                        <h4 style={{ color: 'black' }}>
                          <FaGithub />
                        </h4>
                      </a>
                    </Col>
                    <Col>
                      <a
                        href=""
                      >
                        <h4 style={{ color: 'black' }}>
                          <FaTwitter />
                        </h4>
                      </a>
                    </Col>
                  </Row>
                </Container>

              </Card.Body>

            </Card>
          </Col>
        </Row>
        <Row className="justify-content-md-center" style={{ paddingTop: 50 }}>
          <Col md={4}>
            <Card style={{ width: '18rem' }}>
              <Col style={{ paddingLeft: 50, paddingTop: 13 }} xs={6} md={4}>
                <Image src={naveen} roundedCircle />
              </Col>
              <Card.Body>
                <Card.Title style={{ color: '#45ad97', fontWeight: 'bold', paddingLeft: 50 }}>Naveen B Jacob</Card.Title>
                <Container style={{ paddingTop: 20 }}>
                  <Row>
                    <Col>
                      <a
                        href="https://www.linkedin.com/in/naveen-b-jacob-229a81182/"
                      >
                        <h4 style={{ color: 'black' }}>
                          <FaLinkedin />
                        </h4>
                      </a>

                    </Col>
                    <Col>
                      <a
                        href="https://gitlab.com/NaveenJacob"
                      >
                        <h4 style={{ color: 'black' }}>
                          <FaGithub />
                        </h4>
                      </a>
                    </Col>
                    <Col>
                      <a
                        href="https://twitter.com/naveenbjacob1"
                      >
                        <h4 style={{ color: 'black' }}>
                          <FaTwitter />
                        </h4>
                      </a>
                    </Col>
                  </Row>
                </Container>
              </Card.Body>

            </Card>
          </Col>
          <Col md={4}>
            <Card style={{ width: '18rem' }}>
              <Col style={{ paddingLeft: 50, paddingTop: 13 }} xs={6} md={4}>
                <Image src={gokul} roundedCircle />
              </Col>
              <Card.Body>
                <Card.Title style={{ color: '#45ad97', fontWeight: 'bold', paddingLeft: 50 }}>Gokul Suresh</Card.Title>
                <Container style={{ paddingTop: 20 }}>
                  <Row>
                    <Col>
                      <a
                        href=""
                      >
                        <h4 style={{ color: 'black' }}>
                          <FaLinkedin />
                        </h4>
                      </a>

                    </Col>
                    <Col>
                      <a
                        href="https://github.com/xenomech "
                      >
                        <h4 style={{ color: 'black' }}>
                          <FaGithub />
                        </h4>
                      </a>
                    </Col>
                    <Col>
                      <a
                        href=""
                      >
                        <h4 style={{ color: 'black' }}>
                          <FaTwitter />
                        </h4>
                      </a>
                    </Col>
                  </Row>
                </Container>
              </Card.Body>

            </Card>
          </Col>
        </Row>
      </Container>

      {/*  For the mobile view*/}

      <Container className="d-lg-none" style={{ paddingBottom: 40, paddingLeft: 30 }}>
        <Row style={{ paddingTop: 80 }}>
          <Col style={heights} >
            <Card style={{ width: '18rem' }}>
              <Col style={{ paddingLeft: 50, paddingTop: 13 }} xs={6} md={4}>
                <Image src={leo} roundedCircle />
              </Col>
              <Card.Body>
                <Card.Title style={{ color: '#45ad97', fontWeight: 'bold', paddingLeft: 50 }}>Leo Varghese</Card.Title>
                <Container style={{ paddingTop: 20 }}>
                  <Row>
                    <Col>
                      <a
                        href="https://www.linkedin.com/in/leo-mv-578b13147/"
                      >
                        <h4 style={{ color: 'black' }}>
                          <FaLinkedin />
                        </h4>
                      </a>

                    </Col>
                    <Col>
                      <a
                        href="https://gitlab.com/leo_george "
                      >
                        <h4 style={{ color: 'black' }}>
                          <FaGithub />
                        </h4>
                      </a>
                    </Col>
                    <Col>
                      <a
                        href="https://twitter.com/Leomv55"
                      >
                        <h4 style={{ color: 'black' }}>
                          <FaTwitter />
                        </h4>
                      </a>
                    </Col>
                  </Row>
                </Container>


              </Card.Body>

            </Card>
          </Col>
          <Col style={heights} xs={12} sm={6} md={6}>
            <Card style={{ width: '18rem' }}>
              <Col style={{ paddingLeft: 50, paddingTop: 13 }} xs={6} md={4}>
                <Image src={mohit} roundedCircle />
              </Col>
              <Card.Body>
                <Card.Title style={{ color: '#45ad97', fontWeight: 'bold', paddingLeft: 50 }}>Mohit Rajan E</Card.Title>
                <Container style={{ paddingTop: 20 }}>
                  <Row>
                    <Col>
                      <a
                        href="https://www.youtube.com/c/jamesqquick"
                      >
                        <h4 style={{ color: 'black' }}>
                          <FaLinkedin />
                        </h4>
                      </a>

                    </Col>
                    <Col>
                      <a
                        href="https://www.youtube.com/c/jamesqquick"
                      >
                        <h4 style={{ color: 'black' }}>
                          <FaGithub />
                        </h4>
                      </a>
                    </Col>
                    <Col>
                      <a
                        href="https://www.youtube.com/c/jamesqquick"
                      >
                        <h4 style={{ color: 'black' }}>
                          <FaTwitter />
                        </h4>
                      </a>
                    </Col>
                  </Row>
                </Container>
              </Card.Body>

            </Card>
          </Col>
          <Col style={heights} xs={12} sm={6} md={6}>
            <Card style={{ width: '18rem' }}>
              <Col style={{ paddingLeft: 50, paddingTop: 13 }} xs={6} md={4}>
                <Image src={alen} roundedCircle />
              </Col>
              <Card.Body>
                <Card.Title style={{ color: '#45ad97', fontWeight: 'bold', paddingLeft: 50 }}>Alan Loovees</Card.Title>
                <Container style={{ paddingTop: 20 }}>
                  <Row>
                    <Col>
                      <a
                        href="https://www.linkedin.com/in/alan-loovees-472916169 "
                      >
                        <h4 style={{ color: 'black' }}>
                          <FaLinkedin />
                        </h4>
                      </a>

                    </Col>
                    <Col>
                      <a
                        href="https://gitlab.com/AlanLoovees"
                      >
                        <h4 style={{ color: 'black' }}>
                          <FaGithub />
                        </h4>
                      </a>
                    </Col>
                    <Col>
                      <a
                        href="https://twitter.com/alanloovees"
                      >
                        <h4 style={{ color: 'black' }}>
                          <FaTwitter />
                        </h4>
                      </a>
                    </Col>
                  </Row>
                </Container>

              </Card.Body>

            </Card>
          </Col>

          <Col style={heights} xs={12} sm={6} md={6}>
            <Card style={{ width: '18rem' }}>
              <Col style={{ paddingLeft: 50, paddingTop: 13 }} xs={6} md={4}>
                <Image src={deepak} roundedCircle />
              </Col>
              <Card.Body>
                <Card.Title style={{ color: '#45ad97', fontWeight: 'bold', paddingLeft: 30 }}>Deepak Rao Fletcher</Card.Title>
                <Container style={{ paddingTop: 20 }}>
                  <Row>
                    <Col>
                      <a
                        href="https://www.youtube.com/c/jamesqquick"
                      >
                        <h4 style={{ color: 'black' }}>
                          <FaLinkedin />
                        </h4>
                      </a>

                    </Col>
                    <Col>
                      <a
                        href="https://www.youtube.com/c/jamesqquick"
                      >
                        <h4 style={{ color: 'black' }}>
                          <FaGithub />
                        </h4>
                      </a>
                    </Col>
                    <Col>
                      <a
                        href="https://www.youtube.com/c/jamesqquick"
                      >
                        <h4 style={{ color: 'black' }}>
                          <FaTwitter />
                        </h4>
                      </a>
                    </Col>
                  </Row>
                </Container>
              </Card.Body>

            </Card>
          </Col>
          <Col style={heights} xs={12} sm={6} md={6}>
            <Card style={{ width: '18rem' }}>
              <Col style={{ paddingLeft: 50, paddingTop: 13 }} xs={6} md={4}>
                <Image src={devu} roundedCircle />
              </Col>
              <Card.Body>
                <Card.Title style={{ color: '#45ad97', fontWeight: 'bold', paddingLeft: 50 }}>Devu R Kurup</Card.Title>
                <Container style={{ paddingTop: 20 }}>
                  <Row>
                    <Col>
                      <a
                        href="https://www.youtube.com/c/jamesqquick"
                      >
                        <h4 style={{ color: 'black' }}>
                          <FaLinkedin />
                        </h4>
                      </a>

                    </Col>
                    <Col>
                      <a
                        href="https://www.youtube.com/c/jamesqquick"
                      >
                        <h4 style={{ color: 'black' }}>
                          <FaGithub />
                        </h4>
                      </a>
                    </Col>
                    <Col>
                      <a
                        href="https://www.youtube.com/c/jamesqquick"
                      >
                        <h4 style={{ color: 'black' }}>
                          <FaTwitter />
                        </h4>
                      </a>
                    </Col>
                  </Row>
                </Container>
              </Card.Body>

            </Card>
          </Col>
          <Col style={heights} xs={12} sm={6} md={6}>
            <Card style={{ width: '18rem' }}>
              <Col style={{ paddingLeft: 50, paddingTop: 13 }} xs={6} md={4}>
                <Image src={paul} roundedCircle />
              </Col>
              <Card.Body>
                <Card.Title style={{ color: '#45ad97', fontWeight: 'bold', paddingLeft: 35 }}>Paul Elias Sojan</Card.Title>
                <Container style={{ paddingTop: 20 }}>
                  <Row>
                    <Col>
                      <a
                        href="https://www.linkedin.com/in/paul-elias-sojan-8b9b80177/"
                      >
                        <h4 style={{ color: 'black' }}>
                          <FaLinkedin />
                        </h4>
                      </a>

                    </Col>
                    <Col>
                      <a
                        href="https://gitlab.com/paul.eliassojan"
                      >
                        <h4 style={{ color: 'black' }}>
                          <FaGithub />
                        </h4>
                      </a>
                    </Col>
                    <Col>
                      <a
                        href="https://www.youtube.com/c/jamesqquick"
                      >
                        <h4 style={{ color: 'black' }}>
                          <FaTwitter />
                        </h4>
                      </a>
                    </Col>
                  </Row>
                </Container>

              </Card.Body>

            </Card>
          </Col>

          <Col style={heights} xs={12} sm={6} md={6}>
            <Card style={{ width: '18rem' }}>
              <Col style={{ paddingLeft: 50, paddingTop: 13 }} xs={6} md={4}>
                <Image src={naveen} roundedCircle />
              </Col>
              <Card.Body>
                <Card.Title style={{ color: '#45ad97', fontWeight: 'bold', paddingLeft: 45 }}>Naveen B Jacob</Card.Title>
                <Container style={{ paddingTop: 20 }}>
                  <Row>
                    <Col>
                      <a
                        href="https://www.linkedin.com/in/naveen-b-jacob-229a81182/"
                      >
                        <h4 style={{ color: 'black' }}>
                          <FaLinkedin />
                        </h4>
                      </a>

                    </Col>
                    <Col>
                      <a
                        href="https://gitlab.com/NaveenJacob"
                      >
                        <h4 style={{ color: 'black' }}>
                          <FaGithub />
                        </h4>
                      </a>
                    </Col>
                    <Col>
                      <a
                        href="https://twitter.com/naveenbjacob1"
                      >
                        <h4 style={{ color: 'black' }}>
                          <FaTwitter />
                        </h4>
                      </a>
                    </Col>
                  </Row>
                </Container>
              </Card.Body>

            </Card>
          </Col>
          <Col style={heights} xs={12} sm={6} md={6}>
            <Card style={{ width: '18rem' }}>
              <Col style={{ paddingLeft: 50, paddingTop: 13 }} xs={6} md={4}>
                <Image src={gokul} roundedCircle />
              </Col>
              <Card.Body>
                <Card.Title style={{ color: '#45ad97', fontWeight: 'bold', paddingLeft: 50 }}>Gokul Suresh</Card.Title>
                <Container style={{ paddingTop: 20 }}>
                  <Row>
                    <Col>
                      <a
                        href="https://www.youtube.com/c/jamesqquick"
                      >
                        <h4 style={{ color: 'black' }}>
                          <FaLinkedin />
                        </h4>
                      </a>

                    </Col>
                    <Col>
                      <a
                        href="https://www.youtube.com/c/jamesqquick"
                      >
                        <h4 style={{ color: 'black' }}>
                          <FaGithub />
                        </h4>
                      </a>
                    </Col>
                    <Col>
                      <a
                        href="https://www.youtube.com/c/jamesqquick"
                      >
                        <h4 style={{ color: 'black' }}>
                          <FaTwitter />
                        </h4>
                      </a>
                    </Col>
                  </Row>
                </Container>
              </Card.Body>

            </Card>
          </Col>
        </Row>
      </Container>

      <div style={{ backgroundColor: 'white' }}>
        <br />
        <Row className="justify-content-md-center">
          <Col className="d-block d-sm-block d-md-none">
            <Image src={Gallery} style={{ paddingLeft: '6rem' }} rounded />
          </Col>

          <Col className="d-none d-lg-block" xs={12} md={1}>
            <Image src={Gallery} rounded />
          </Col>
          <div className="gallery" style={{ paddingBottom: 10 }}>
            <div className="gallery-item-one"></div>
            <div className="gallery-item-two"></div>
            <div className="gallery-item-three"></div>
            <div className="gallery-item-four"></div>
            <div className="gallery-item-five"></div>
            <div className="gallery-item-six"></div>
          </div>
        </Row>
      </div>

    </div>
  );
};
const teamStyle = {
  paddingTop: 70,
  backgroundColor: '#ebebeb',
  paddingBottom: 20
}
const heights = {
  paddingTop: 20
}
export default Example;