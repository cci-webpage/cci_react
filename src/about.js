import React from 'react';
import {
    Container, Row, Col, Card, CardImg, CardText, CardBody,
    CardTitle,
} from 'reactstrap';
import { Image,Button,ButtonToolbar } from 'react-bootstrap';

import Gallery from './gallery.png';

import banner from './cover.jpg'; 
const App = (props) => {
    return (
        
        <div>
            

            <Row>
            <Col xs="12" sm="6" style={{backgroundColor:'#203475',paddingLeft:50}}>
                <br/> <br/> <br/> <br/> <br/><br/><br/><br/>
                <Col md={{ span: 6, offset:3 }}>
                <h1 style={{fontSize:70,color:'white',fontWeight:'bold'}}>
                    
                    WHO
                </h1>
                
                <h1 style={{fontSize:70,color:'white',fontWeight:'bold'}}>
                    
                    WE ARE?
                </h1>
                
                <h5 style={{paddingRight:60,paddingTop:20,color:'white'}}>
                It is a long established fact that a reader will be distracted by the readable content of a page when looking at 
                its layout. The point of using Lorem Ipsum is that it has normal.
                </h5>
                <ButtonToolbar style={{paddingTop:20}}>
                
                </ButtonToolbar>
                <br/>
    </Col>
                
         </Col>
         <Col className="d-none d-sm-block" xs="12" sm="6">
         <Image style={{paddingTop:10,width:'55rem'}} rounded /> 
         </Col>
         <Col className="d-none d-xs-block mobile_view" xs="12" sm="6">
         <Image style={{paddingTop:10,width:'55rem'}} rounded /> 
         </Col>
           </Row>
           <head>
               <h1>
                   WHO
               </h1>
           </head>
            <br /><br /><br /><br /><br />
            <Container>
                <Row>
                    <Col xs="12" sm="3">
                        <Card>
                            <CardImg top width="100%"  alt="Card image cap" />
                            <CardBody>
                                <CardTitle>Vision</CardTitle>
                                <CardText>Thus by equipping individuals with future technologies, the foundation aims at creating a potent workforce, 
                                    capable of unlocking the vast reservoir of untapped potential, in the form of creativity and innovation that the 
                                    youth of the 21st century holds. Choice is the greatest luxury one can have; and to exercise that, it is only 
                                    imperative that we arm each and every student with the right options to choose.
                                </CardText>
                            </CardBody>
                        </Card>

                    </Col>
                    <br />
                    <Col xs="12" sm="3">
                        <Card>
                            <CardImg top width="100%"  alt="Card image cap" />
                            <CardBody>
                                <CardTitle>Vision</CardTitle>
                                <CardText>Thus by equipping individuals with future technologies, the foundation aims at creating a potent workforce, 
                                    capable of unlocking the vast reservoir of untapped potential, in the form of creativity and innovation that the 
                                    youth of the 21st century holds. Choice is the greatest luxury one can have; and to exercise that, it is only 
                                    imperative that we arm each and every student with the right options to choose.
                                </CardText>
                            </CardBody>
                        </Card>

                    </Col>
                    <br />
                    <Col xs="12" sm="3">
                        <Card>
                            <CardImg top width="100%" alt="Card image cap" />
                            <CardBody>
                                <CardTitle>Vision</CardTitle>
                                <CardTitle>Vision</CardTitle>
                                <CardText>Thus by equipping individuals with future technologies, the foundation aims at creating a potent workforce, 
                                    capable of unlocking the vast reservoir of untapped potential, in the form of creativity and innovation that the 
                                    youth of the 21st century holds. Choice is the greatest luxury one can have; and to exercise that, it is only 
                                    imperative that we arm each and every student with the right options to choose.
                                </CardText>
                            </CardBody>
                        </Card>

                    </Col>
                    <br />
                    <Col xs="12" sm="3">
                        <Card>
                            <CardImg top width="100%"  alt="Card image cap" />
                            <CardBody>
                                <CardTitle>Vision</CardTitle>
                                <CardTitle>Vision</CardTitle>
                                <CardText>Thus by equipping individuals with future technologies, the foundation aims at creating a potent workforce, 
                                    capable of unlocking the vast reservoir of untapped potential, in the form of creativity and innovation that the 
                                    youth of the 21st century holds. Choice is the greatest luxury one can have; and to exercise that, it is only 
                                    imperative that we arm each and every student with the right options to choose.
                                </CardText>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </Container>

            <br /><br /><br /><br /><br />
            <Row className="justify-content-md-center">
                <Image src={Gallery} style={{ paddingLeft: 80 }} rounded />
                <div className="gallery" style={{ paddingBottom: 10 }}>
                    <div className="gallery-item-one"></div>
                    <div className="gallery-item-two"></div>
                    <div className="gallery-item-three"></div>
                    <div className="gallery-item-four"></div>
                    <div className="gallery-item-five"></div>
                    <div className="gallery-item-six"></div>
                </div>
            </Row>
        </div>

    );

}
export default App;
